centos-dev
==========

CentOS Docker_ image with development tools and extra libraries needed to compile the RT kernel.
It can be used to create RPMs.

.. _Docker: https://www.docker.com
