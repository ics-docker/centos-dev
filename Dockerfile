FROM centos:7.8.2003

LABEL maintainer "benjamin.bertrand@esss.se"

RUN yum update -y \
  && yum groups mark convert \
  && yum groups install -y "Development Tools" \
  && yum install -y \
     epel-release \
  && yum install -y \
     rpmbuild \
     rpmdevtools \
     yum-utils \
     mercurial \
     ncurses-devel \
     hmaccalc \
     zlib-devel \
     binutils-devel \
     elfutils-libelf-devel \
     net-tools \
     bc \
     pesign \
     xmlto \
     asciidoc \
     which \
     openssl \
     desktop-file-utils \
     python-devel \
     newt-devel \
     perl-ExtUtils-Embed \
     tree \
     libpcap-devel \
     libevent-devel \
     mariadb-devel \
     sqlite-devel \
     automake \
     cmake \
     epel-rpm-macros \
     libtelnet-devel \
  && yum clean all \
  && rm -rf /var/cache/yum

RUN useradd -m -s /bin/bash -u 1000 csi

USER csi
